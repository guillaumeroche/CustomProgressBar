package guillaumeroche.customprogressbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class TextProgressBar extends ProgressBar {

	private String mText = "";
	private Paint mTextPaint = new Paint();
	private Rect mBounds = new Rect();

	public TextProgressBar(Context context) {
		super(context);
		initPaint(null);
	}

	public TextProgressBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initPaint(attrs);
	}

	public TextProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPaint(attrs);
	}
	
	private void initPaint(AttributeSet attrs) {
		mTextPaint.setAntiAlias(true);
		if (attrs == null) {
			mTextPaint.setColor(Color.BLACK);
			mTextPaint.setTextSize(15);
		}
		else {
			TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextProgressBar, 0, 0);
			setText(a.getString(R.styleable.TextProgressBar_text));
			setTextColor(a.getColor(R.styleable.TextProgressBar_textColor, Color.BLACK));
			setTextSize(a.getDimension(R.styleable.TextProgressBar_textSize, 15));
			a.recycle();
		}
	}

	@Override
	public synchronized void setProgress(int progress) {
		super.setProgress(progress);
		setText(String.valueOf(progress));
	}

	@Override
	protected synchronized void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		// In order to show text in a middle, we need to know its size
		mTextPaint.getTextBounds(mText, 0, mText.length(), mBounds);
		// Now we store font size in bounds variable and can calculate it's
		// position
		int x = getWidth() / 2 - mBounds.centerX();
		int y = getHeight() / 2 - mBounds.centerY();
		// drawing text with appropriate color and size in the center
		canvas.drawText(mText, x, y, mTextPaint);
	}

	public String getText() {
		return mText;
	}

	public synchronized void setText(String text) {
		if (text != null) {
			this.mText = text;
		} else {
			this.mText = "";
		}
		postInvalidate();
	}

	public int getTextColor() {
		return mTextPaint.getColor();
	}

	public synchronized void setTextColor(int textColor) {
		mTextPaint.setColor(textColor);
		postInvalidate();
	}

	public float getTextSize() {
		return mTextPaint.getTextSize();
	}

	public synchronized void setTextSize(float textSize) {
		mTextPaint.setTextSize(textSize);
		postInvalidate();
	}

}
