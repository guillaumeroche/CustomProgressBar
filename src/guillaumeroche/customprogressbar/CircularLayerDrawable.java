package guillaumeroche.customprogressbar;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class CircularLayerDrawable extends LevelLayerDrawable {

	public CircularLayerDrawable(Drawable[] layers) {
		super(layers);
		onLevelChange(getLevel());
	}

	@Override
	public void draw(Canvas canvas) {
		
		int drawableCount = getNumberOfLayers();
		
		if (drawableCount <= 1) {
			// We cannot lay out one child drawable on a circle, so here just call the draw
			// method from super class
			super.draw(canvas);
			return;
		}
		
		// First determine the size of the drawing region
		
		// External radius
		int maxRadius = Math.min(canvas.getHeight(), canvas.getWidth()) / 2;
		
		// Angle delta between 2 drawables
		double angle = 2 * Math.PI / drawableCount;
		
		// Apply the Al-Kashi theorem to determine the distance between the center of adjacent drawables
		// Divide it by 2 to determine the drawable size, so they don't overlap
		// Divide by 2 again the get the half-size
		int halfDrawableSize = (int) (maxRadius * Math.sqrt(2 * (1 - Math.cos(angle))) / 4);
		
		// The radius itself
		int radius = maxRadius - halfDrawableSize;
		
		// Translate the canvas in the first Drawable region
		int left = canvas.getWidth() / 2;
		int top = (canvas.getHeight() / 2) - radius;
		
		Rect bounds = new Rect(-halfDrawableSize, -halfDrawableSize, halfDrawableSize, halfDrawableSize * 3);
		canvas.translate(left, top);
		
		// The first displayed Drawable is on top, so it's angle on the circle is PI/2
		double currentAngle = Math.PI / 2;
		
		for (int i = 0; i < drawableCount; i++) {
			
			Drawable currentDrawable = getDrawable(i);
			currentDrawable.setBounds(bounds);
			
			// Save the canvas state
			canvas.save();
			
			// Rotate the canvas
			canvas.rotate((float) (i * Math.toDegrees(angle)));
			currentDrawable.draw(canvas);
			canvas.restore();
			
			// translate the drawing region along the circle
			
			double nextAngle = currentAngle + angle;
			float dx = (float) (Math.cos(currentAngle) - Math.cos(nextAngle)) * radius;
			float dy = (float) (Math.sin(currentAngle) - Math.sin(nextAngle)) * radius;
			canvas.translate(dx, dy);
									
			currentAngle = nextAngle;
			
		}
		
	}

}
