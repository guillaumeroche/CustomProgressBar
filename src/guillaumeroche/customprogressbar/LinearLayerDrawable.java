package guillaumeroche.customprogressbar;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class LinearLayerDrawable extends LevelLayerDrawable {

	public LinearLayerDrawable(Drawable[] layers) {
		super(layers);
		onLevelChange(getLevel());
	}

	@Override
	public void draw(Canvas canvas) {
		
		int drawableCount = getNumberOfLayers();
		
		// Protection against division by 0
		if (drawableCount == 0) {
			return;
		}
		
		int drawableWidth = canvas.getWidth() / drawableCount;
		
		// Prepare the drawing region
		Rect bounds = new Rect(0, 0, drawableWidth, canvas.getHeight());
		
		for (int i = 0; i < drawableCount; i++) {
						
			Drawable currentDrawable = getDrawable(i);
			currentDrawable.setBounds(bounds);
			currentDrawable.draw(canvas);
			
			// Translate the drawing region
			canvas.translate(drawableWidth, 0);
		}
	}
	
	
	
}
