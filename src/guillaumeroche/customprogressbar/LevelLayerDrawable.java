package guillaumeroche.customprogressbar;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

public class LevelLayerDrawable extends LayerDrawable {

	/**
	 * Constructor
	 * @param layers array of drawables
	 */
	public LevelLayerDrawable(Drawable[] layers) {
		super(layers);
	}
	
	@Override
	/**
	 * Override the onLevelChange
	 * Depending on the level value, the first n% drawables will be opaque
	 * The next drawable will be  a varying opacity
	 * The following drawables will be transparent
	 */
	protected boolean onLevelChange(int level) {
		super.onLevelChange(level);
		
		int drawableCount = getNumberOfLayers();
		int displayCount = level * drawableCount / 10000;
		
		float percent = (float) level * drawableCount / 10000;
		percent -= (float) displayCount;
		
		for (int i = 0; i < drawableCount; i++) {
			if (i < displayCount) {
				getDrawable(i).setAlpha(255);
			}
			
			if (i == displayCount) {
				getDrawable(i).setAlpha((int) (255 * percent));
			}
			
			if (i > displayCount) {
				getDrawable(i).setAlpha(Color.TRANSPARENT);
			}
		}
		
		return true;
	}

}
