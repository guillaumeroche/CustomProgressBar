package guillaumeroche.customprogressbar;

import java.util.ArrayList;
import java.util.List;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class MainActivity extends Activity {
	
	private final String KEY_INDEX = "index";
	
	private List<Drawable> mDrawableList;
	private int mCurrentDrawableIndex = 0;
	private TextProgressBar mProgressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Restore the saved index if available
		if (savedInstanceState != null) {
			mCurrentDrawableIndex = savedInstanceState.getInt(KEY_INDEX);
		}
		
		setContentView(R.layout.activity_main);
		
		// Get the progress bar
		mProgressBar = (TextProgressBar) findViewById(R.id.progressBar);
		
		// Get the seekbar
		final SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
		
		// Define an animator
		final ObjectAnimator animator = new ObjectAnimator();
		animator.setTarget(mProgressBar);
		animator.setPropertyName("progress");
		animator.setInterpolator(new AnticipateOvershootInterpolator());
		animator.setDuration(500);
		
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				animator.setIntValues(mProgressBar.getProgress(), progress);
				animator.start();
			}
		});
		
		Drawable[] squares = new Drawable[24];
		for (int i = 0; i < squares.length; i++) {
			Drawable d = getResources().getDrawable(R.drawable.rectangle);
			squares[i] = d;
		}
		
		Drawable[] dots = new Drawable[12];
		for (int i = 0; i < dots.length; i++) {
			Drawable d = getResources().getDrawable(R.drawable.dot);
			dots[i] = d;
		}
		
		// Fill the Drawable List
		mDrawableList = new ArrayList<Drawable>();
		
		// Default Drawable
		mDrawableList.add(mProgressBar.getProgressDrawable());
		
		// Ring
		mDrawableList.add(getResources().getDrawable(R.drawable.progress_circle));
		
		mDrawableList.add(getResources().getDrawable(R.drawable.progress_pie));
		
		mDrawableList.add(new CircularLayerDrawable(squares));
		
		mDrawableList.add(new LinearLayerDrawable(dots));
		
		mProgressBar.setProgressDrawable(mDrawableList.get(mCurrentDrawableIndex));
	}

	@Override
	/**
	 * Save the current drawable index
	 */
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_INDEX, mCurrentDrawableIndex);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
	    switch (item.getItemId()) {
	        case R.id.action_next:
	        	setNextProgressDrawable();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }

	}
	
	private void setNextProgressDrawable() {
		mCurrentDrawableIndex++;
		if (mCurrentDrawableIndex == mDrawableList.size()) {
			mCurrentDrawableIndex = 0;
		}
		
		mProgressBar.setProgressDrawable(mDrawableList.get(mCurrentDrawableIndex));
		
		// Update Progress Bar
		int currentProgress = mProgressBar.getProgress();
		mProgressBar.setProgress(0);
		mProgressBar.setProgress(currentProgress);
	}
	
	

}
